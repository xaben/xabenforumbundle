<?php

namespace Xaben\ForumBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Request;

class PostRepository extends EntityRepository
{
    public function findAllByPage(Request $request, $topicId, $page, $paginator)
    {
        $qb = $this
            ->createQueryBuilder('p')
            ->select('p, pt, u, b')
            ->innerJoin('p.topic', 't')
            ->innerJoin('p.posttext', 'pt')
            ->innerJoin('p.poster', 'u')
            ->innerJoin('u.baseuser', 'b')
            ->where('t.id = :topicId')
            ->setParameter('topicId', $topicId)
            ->orderBy('t.id', 'DESC');

        $pagination = $paginator->paginate(
                $qb->getQuery(),
                $request->query->get('page', $page),
                10
        );

        return $pagination;
    }
}
