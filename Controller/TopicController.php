<?php

namespace Xaben\ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Xaben\ForumBundle\Form\Type\TopicType;

class TopicController extends Controller
{
    /**
     * Display and paginate all topics for a given forum
     *
     * @param Request $request
     * @param integer $forumId
     * @param integer $page
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(Request $request, $forumId, $page)
    {
        //get topics from current page
        $topics = $this
            ->get('xaben.forum.repository.topic')
            ->findAllByPage($request, $forumId, $page, $this->get('knp_paginator'));

        $forum = $this
            ->get('xaben.forum.repository.forum')
            ->findOneById($forumId);

        return $this->render('XabenForumBundle:Default:topics.html.twig', ['topics' => $topics, 'forum' => $forum]);
    }

    /**
     * Display form for creating a new topic in a given forum
     *
     * @param Request $request
     * @param integer $forumId
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, $forumId)
    {
        //check if user logged in
        if (false === $this->get('security.context')->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException();
        }

        //render form
        $topicManager = $this->get('xaben.forum.topicmanager');
        $topic        = $topicManager->getNewTopic($forumId);
        $form         = $this->createForm(TopicType::class, $topic);

        $form->handleRequest($request);
        if ($form->isValid()) {
            foreach ($topic->getPosts() as $post) {
                $this->get('xaben.forum.postmanager')->save($post);
            }

            $topicManager->saveTopic($topic);

            return $this->redirectToRoute('XabenForumBundle_topics', array('forumId' => $forumId));
        }

        return $this->render(
            'XabenForumBundle:Default:newtopic.html.twig', [
            'form'    => $form->createView(),
            'forumId' => $forumId,
        ]);
    }
}
