<?php

namespace Xaben\ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * Display forum index
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $categories = $this
            ->get('xaben.forum.repository.category')
            ->findAllWithForums();

        return $this->render('XabenForumBundle:Default:index.html.twig', array('categories'=>$categories));
    }
}
