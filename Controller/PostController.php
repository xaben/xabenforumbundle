<?php

namespace Xaben\ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Xaben\ForumBundle\Form\Type\PostType;
use Symfony\Component\HttpFoundation\Request;

class PostController extends Controller
{
    /**
     * Display and paginate all posts for a given topic
     *
     * @param Request $request
     * @param integer $topicId
     * @param integer $page
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(Request $request, $topicId, $page)
    {
        $topic = $this->get('xaben.forum.repository.topic')->find($topicId);

        //get topics from current page
        $posts = $this
            ->get('xaben.forum.repository.post')
            ->findAllByPage($request, $topicId, $page, $this->get('knp_paginator'));

        return $this->render(
            'XabenForumBundle:Default:posts.html.twig',
            [
                'posts' => $posts,
                'topic' => $topic
            ]
        );

    }

    /**
     * Display form for creating a new post in a given topic
     *
     * @param Request $request
     * @param integer $topicId
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, $topicId)
    {
        //check if user logged in
        if (false === $this->get('security.context')->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException();
        }

        $postManager = $this->get('xaben.forum.postmanager');
        $post = $postManager->getNewPost($topicId);

        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $postManager->save($post);

            return $this->redirectToRoute(
                'XabenForumBundle_posts',
                [
                    'topicId' => $topicId
                ]
            );
        }

        return $this->render(
            'XabenForumBundle:Default:newpost.html.twig',
            [
                'form' => $form->createView(),
                'topicId' => $topicId,
            ]
        );
    }
}
