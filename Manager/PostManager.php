<?php

namespace Xaben\ForumBundle\Manager;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Xaben\ForumBundle\Entity\Post;
use Xaben\ForumBundle\Manager\UserManager;
use Symfony\Component\HttpFoundation\Request;

class PostManager
{
    private $em;
    private $usermanager;
    private $request;

    /**
     * @param EntityManager $em
     * @param UserManager $usermanager
     * @param RequestStack $requestStack
     */
    public function __construct(EntityManager $em, UserManager $usermanager, RequestStack $requestStack)
    {
        $this->em = $em;
        $this->usermanager = $usermanager;
        // $this->forummanager = $forummanager;
        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     * Returns an empty Post object for use in forms
     *
     * @param integer $topicId
     * @return Post
     */
    public function getNewPost($topicId = null)
    {
        //create new Post
        $post = new Post();
        $post->setIp($this->request->getClientIp());
        $post->setPostTime(new \DateTime);
        $post->setPoster($this->usermanager->getCurrentUser());

        //get topic
        if (null !== $topicId) {
            $topic = $this->em->getRepository('XabenForumBundle:Topic')
                ->findOneById($topicId);
            $post->setTopic($topic);
        }

        return $post;
    }

    public function save(Post $post)
    {
        $this->em->persist($post);
        $this->em->flush();
    }
}
